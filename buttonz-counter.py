from urllib.request import urlopen
from lxml import etree
import sys, re, os
import urllib.robotparser
from urllib.parse import urlparse


class Website:
    def __init__(self, url):
        x = urlparse(url)
        self.scheme = 'https://'
        self.netloc = x.netloc or x.scheme or x.path
        if self.netloc != x.path:
            self.path = x.path
            self.params = x.params
            self.query = x.query
            self.fragment = x.fragment
        else:
            self.path = x.params
            self.params = x.query
            self.query = x.fragment
        self.port = x.port
        self.buttonz = 0
        if x.scheme not in ['https', 'http']:
            self.full_url = self.scheme + x.geturl()
        else:
            self.full_url = x.geturl()
        self.valid = self.validate_url()
        self.robots = self.check_robots()

    def check_robots(self):
        '''Check robots.txt file for each website. Return True if file not detected.'''
        if self.valid is False:
            return False

        robots = urllib.robotparser.RobotFileParser()
        try:
            robots.set_url(self.scheme + self.netloc + "/robots.txt")
            robots.read()
        except:
            return True

        if robots.can_fetch("*", self.full_url):
            return True
        else:
            sys.stderr.write('Scrapping data is not allowed for: ' + self.full_url + '\n')
            return False

    def validate_url(self):
        '''Validate urls using re.'''
        regex = re.compile(
            r'^(?:http|ftp)s?://'  # http:// or https://
            r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain
            r'localhost|'  # localhost
            r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ip
            r'(?::\d+)?'  # optional port
            r'(?:/?|[/?]\S+)$', re.IGNORECASE)

        if re.match(regex, self.full_url):
            return True
        else:
            sys.stderr.write('Invalid url ' + self.full_url + '\n')
            return False


class ButtonzCounter:
    '''Counting buttons on websites provided in txt file.'''

    def __init__(self):
        self.urls_to_check = []

    def check_urls(self, input_file):
        if self.check_file(input_file):
            with open(input_file, 'r') as f:
                for line in f:
                    self.urls_to_check.append(Website(line.strip()))


    def count_buttons(self):
        '''Counts buttons on website, using xpath. Returns 0 for timeout, 404 or if check_robots return False.'''
        for website in self.urls_to_check:
            if website.valid and website.robots:
                try:
                    response = urlopen(website.full_url, timeout=10)
                except:
                    sys.stderr.write('Unable to request provided URL ' + website.full_url + '\n')
                    continue
                try:
                    htmlparser = etree.HTMLParser()
                    tree = etree.parse(response, htmlparser)
                    buttons = tree.xpath("//button | //input[translate(@type, 'BIMSTU', 'bimstu')='submit'] | "
                                        "//input[translate(@type, 'ERST', 'erst')='reset'] | "
                                        "//input[translate(@type, 'BONTU', 'bontu')='button'] | "
                                        "//a[contains(translate(@class, 'BTN', 'btn'), 'btn')] | "
                                        "//a[contains(translate(@class, 'BONTU', 'bontu'), 'button')]")
                    website.buttonz = len(buttons)
                except:
                    sys.stderr.write('Unable to parse provided URL ' + website.full_url + '\n')

    def write_results(self, output_file):
        '''Writes output to csv file.'''
        with open(output_file, 'w+') as o:
            o.write('address,number_of_buttons\n')
            for website in self.urls_to_check:
                o.write(','.join([website.full_url,str(website.buttonz)])+'\n')

    def check_file(self, file):
        '''Validate provided file.'''
        try:
            if file.lower().endswith('.txt') and os.path.getsize(file) > 0:
                return True
            else:
                sys.stderr.write('Invalid data file extension.\n')
                return False
        except:
            sys.stderr.write('File not found.\n')
            return False


if __name__ == '__main__':
    robots = urllib.robotparser.RobotFileParser()
    button = ButtonzCounter()
    button.check_urls(sys.argv[1])
    button.count_buttons()
    button.write_results(sys.argv[2])