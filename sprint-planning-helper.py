import csv, sys


class Task:
    def __init__(self, index, story_points, ksp):
        self.index = index
        self.story_points = story_points
        self.ksp = ksp


class Knapsack:
    '''Objective Knapsack Problem implementation.'''
    def __init__(self, file, velocity):
        self.tasks = []
        self.file_to_read = file
        self.picked_tasks = []
        self.velocity = velocity
        self.table = []

    def csv_reader(self):
        '''Reading tasks from provided file. Creating Task object for each task.'''
        with open(self.file_to_read,'r', newline='') as csvfile:
            data = csv.reader(csvfile, delimiter=',')
            for index, row in enumerate(data):
                if row[0] not in 'task_id':
                    #Testing if provided varibles are posivite integers
                    if int(row[0]) >= 0 and int(row[1]) >= 0 and int(row[2]) >= 0:
                        self.tasks.append(Task(int(row[0]), int(row[1]), int(row[2])))
                    else:
                        raise Exception('Invalid data at line ' + str(index + 1))

    def knapsack(self):
        '''Implementatnion of Dynamic Programming Knapsack Problem.
        source: https://codereview.stackexchange.com/a/125386 '''

        self.table = [[0 for x in range(self.velocity + 1)] for x in range(len(self.tasks) + 1)]

        for next_idx, (task, weights) in enumerate(zip(self.tasks, self.table), 1):
            for w, current_weight in enumerate(weights[1:], 1):
                if task.story_points <= w:
                    self.table[next_idx][w] = max(
                        task.ksp + weights[w - task.story_points], current_weight)
                else:
                    self.table[next_idx][w] = current_weight

    def fetch_items(self):
        #Fetching picked tasks
        for task, weights_p, weights_n in zip(self.tasks[::-1], self.table[-2::-1], self.table[::-1]):
            if weights_n[self.velocity] != weights_p[self.velocity]:
                self.picked_tasks.append(task.index)
                self.velocity -= task.story_points

    def print_results(self):
        sys.stdout.write(', '.join(str(x) for x in sorted(self.picked_tasks))+ '\n')

    def run_module(self):
        self.csv_reader()
        self.knapsack()
        self.fetch_items()
        self.print_results()

def test_input():
    '''Testing provided file and velocity.'''
    if not sys.argv[2].isdigit():
        return sys.stderr.write('Velocity should be integer')
    if int(sys.argv[2]) <= 0:
        return sys.stderr.write('Need more veloctiy.')
    try:
        with open(sys.argv[1], 'r') as file:
            dialect = csv.Sniffer().sniff(file.read(2))
            file.seek(0)
    except:
        return sys.stderr.write('Invalid file format or file not found.')
    return True

if __name__ == '__main__':
    if test_input() is True:
        knap = Knapsack(sys.argv[1], int(sys.argv[2]))
        knap.run_module()