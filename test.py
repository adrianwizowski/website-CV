import unittest, io, sys
from sprint-planning-helper import Knapsack, Task
from buttonz-counter import ButtonzCounter, Website

class TestCasesInternTasks(unittest.TestCase):
    def test_sprint_planning_helper(self):
        try:
            saved_stdout = sys.stdout
            out = io.StringIO()
            sys.stdout = out
            knapsack = Knapsack('test_tasks.csv', 13)
            knapsack.run_module()
            output = out.getvalue()
            assert output == '2, 4, 6\n'
        finally:
            sys.stdout = saved_stdout

    def test_url_validation(self):
        self.assertEqual(url1.validate_url(), True)
        self.assertEqual(url2.validate_url(), False)
        self.assertEqual(url3.validate_url(), True)
        self.assertEqual(url4.validate_url(), True)
        self.assertEqual(url5.validate_url(), True)

    def test_check_file(self):
        self.assertEqual(button.check_file('test.csv'), False)
        self.assertEqual(button.check_file('test.txt'), True)
        self.assertEqual(button.check_file('files_with_websites.tt'), False)

    def test_check_robots(self):
        url1.validate_url()
        url2.validate_url()
        url3.validate_url()
        url4.validate_url()
        self.assertEqual(url1.check_robots(), False)
        self.assertEqual(url2.check_robots(), False)
        self.assertEqual(url3.check_robots(), True)
        self.assertEqual(url4.check_robots(), True)
        self.assertEqual(url5.check_robots(), True)

    def test_count_buttonz(self):
        button.count_buttons()
        self.assertEqual(url1.buttonz, 0)
        self.assertEqual(url2.buttonz, 0)
        self.assertEqual(url3.buttonz, 0)
        self.assertEqual(url4.buttonz, 0)
        self.assertEqual(url5.buttonz, 1)

if __name__ == '__main__':
    button = ButtonzCounter()
    url1 = Website('google.com')
    url2 = Website('google')
    url3 = Website('www.hottestdeals.pl')
    url4 = Website('localhost')
    url5 = Website('www.boredbutton.com')
    button.urls_to_check.append(url1)
    button.urls_to_check.append(url2)
    button.urls_to_check.append(url3)
    button.urls_to_check.append(url4)
    button.urls_to_check.append(url5)
    unittest.main()