# Hello_Again_ClearCode

These are my Python Intern tasks.

## Task 1 - sprint-planning-helper.py

Helps to find the most efficient set of tasks to be taken from the pool of groomed tasks
using 0/1 knapsack problem algorithm.

Command line usage:
```
sprint-planning-helper.py csv_with_tasks.csv 13
```

## Task 2 - buttonz-counter.py

Counting the buttons on websites using lxml lib and xpath. Addresses should be provided in a TXT file.
Writes output to CSV file.

Command line usage:
```
buttonz-counter.py files_with_websites output_file.csv
```

Return 0 for websites that are unable to request, or if scrapping is not allowed.


## Build with:

- Python 3.6

## Authors

- **Adrian Wizowski** - [adrianwizowski](https://github.com/adrianwizowski)